# wh-delay

Remote control of [WoodenHaptics](http://www.woodenhaptics.org/) manipulator with control delay.

## Project parts

* **MatlabModel**
  * Model of articultaed manipultor.
  * Parameters are taken from C++ programm.

## Need to do

* Make integration with CoppeliaSim simulator.
