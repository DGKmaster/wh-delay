function [robot] = model()

%! 1 Initialize all variables
%! %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% 1.1 Haptic device parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Link lengths
L_1 = 0.08;
L_2 = 0.205;
L_3 = 0.2;

%% Diameters
diam_1 = 0.16;
diam_2 = 0.12;
diam_3 = 0.12;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% 1.2 Denavit-Hartenberg parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Link offsets
d_1 = L_1;
d_2 = 0;
d_3 = 0;

%% Link lengths
a_1 = 0;
a_2 = L_2;
a_3 = L_3;

%% Link twists
alpha_1 = pi / 2;
alpha_2 = 0;
alpha_3 = 0;

%% Kinematic joint coordinate offsets
offset_1 = pi / 2;
offset_2 = 0;
offset_3 = 0;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% 1.3 Dynamic parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Link mass
m_1 = 0.14;
m_2 = 0.17;
m_3 = 0.11;

%% Link COG with respect to link coordinate frame
r_1 = [0 0 0.71];
r_2 = [0.051 0 0];
r_3 = [0.091 0 0];

%% Link inertia tensor
I_1 = diag([(m_1 * (diam_1/2)^2)/2 ...
            (m_1 * (3 * (diam_1/2)^2 + L_1^2)) / 12 ...
            (m_1 * (3 * (diam_1/2)^2 + L_1^2)) / 12]);

I_2 = diag([(m_2 * (diam_2 / 2)^2) / 2 ...
            (m_2 * (3 * (diam_2/2)^2 + L_2^2)) / 12 ...
            (m_2 * (3 * (diam_2/2)^2 + L_2^2)) / 12]);

I_3 = diag([(m_3 * (diam_3 / 2)^2)/2 ...
            (m_3 * (3 * (diam_3 / 2)^2 + L_3^2)) / 12 ...
            (m_3 * (3 * (diam_3 / 2)^2 + L_3^2)) / 12]);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% 1.4 General parameters 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Gravity vector
g = [0; 0; 9.81];

G = 1;

%% Friction
B = 0;
Tc = [0 0];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%! %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%! 2 Calculation part
%! %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% 2.1 Setup robot using DH parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Create links based on DH parameters.
L_1 = Link('d',d_1, 'a',a_1, 'alpha',alpha_1, ...
           'offset',offset_1, 'm',m_1, 'I',I_1, ...
           'r',r_1, 'G',G, 'B',B, 'Tc',Tc);

L_2 = Link('d',d_2, 'a',a_2, 'alpha',alpha_2, ...
           'offset',offset_2, 'm',m_2, 'I',I_2, ...
           'r',r_2, 'G',G, 'B',B, 'Tc',Tc);

L_3 = Link('d',d_3, 'a',a_3, 'alpha',alpha_3, ...
           'offset',offset_3, 'm',m_3, 'I',I_3, ...
           'r',r_3, 'G',G, 'B',B, 'Tc',Tc);

%% Create manipulator based on links.
robot = SerialLink([L_1 L_2 L_3], 'name','WoodenHaptics', 'gravity',g);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%! %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
