% Make sure to have the server side running in CoppeliaSim: 
% in a child script of a CoppeliaSim scene, add following command
% to be executed just once, at simulation start:
%
% simRemoteApi.start(19999)
%
% then start simulation, and run this program.
%
% IMPORTANT: for each successful call to simxStart, there
% should be a corresponding call to simxFinish at the end!

classdef coppeliaSim
properties
    sim;
    clientID;
end

methods
    function obj = coppeliaSim()
        disp('Program started');
        
        obj.sim = remApi('remoteApi'); % using the prototype file (remoteApiProto.m)
        obj.sim.simxFinish(-1); % just in case, close all opened connections
        obj.clientID = obj.sim.simxStart('127.0.0.1', 19997, true, true, 5000, 5);

        if (obj.clientID > -1)
            disp('Connected to remote API server');
        else
            disp('Failed connecting to remote API server');
            obj.sim.delete(); % call the destructor!
        end
    end

    function run(obj)
        % Now try to retrieve data in a blocking fashion (i.e. a service call):
        [res, objs] = obj.sim.simxGetObjects(obj.clientID, obj.sim.sim_handle_all, obj.sim.simx_opmode_blocking);
        if (res == obj.sim.simx_return_ok)
            fprintf('Number of objects in the scene: %d\n', length(objs));
        else
            fprintf('Remote API function call returned with error code: %d\n', res);
        end
            
        pause(2);
        
        % [returnCode, handle] = obj.sim.simxGetObjectHandle(obj.clientID, "uarm_motor3#", obj.sim.simx_opmode_streaming);
        % obj.sim.simxGetJointPosition(obj.clientID, handle, obj.sim.simx_opmode_streaming);
        
        % Now retrieve streaming data (i.e. in a non-blocking fashion):
        t = clock;
        startTime = t(6);
        currentTime = t(6);
        obj.sim.simxGetIntegerParameter(obj.clientID, obj.sim.sim_intparam_mouse_x, obj.sim.simx_opmode_streaming); % Initialize streaming
        while (currentTime - startTime < 5)   
            [returnCode, data] = obj.sim.simxGetIntegerParameter(obj.clientID, obj.sim.sim_intparam_mouse_x, obj.sim.simx_opmode_buffer); % Try to retrieve the streamed data
            if (returnCode == obj.sim.simx_return_ok) % After initialization of streaming, it will take a few ms before the first value arrives, so check the return code
                fprintf('Mouse position x: %d\n',data); % Mouse position x is actualized when the cursor is over CoppeliaSim's window
            end
            t = clock;
            currentTime = t(6);
        end
        
        % Now send some data to CoppeliaSim in a non-blocking fashion:
        obj.sim.simxAddStatusbarMessage(obj.clientID,'Hello CoppeliaSim!', obj.sim.simx_opmode_oneshot);
    end

    function delete(obj)
        % Before closing the connection to CoppeliaSim, make sure that the last command sent out had time to arrive. You can guarantee this with (for example):
        obj.sim.simxGetPingTime(obj.clientID);

        % Now close the connection to CoppeliaSim:    
        obj.sim.simxFinish(obj.clientID);
        
        disp('Program ended');
        obj.sim.delete(); % call the destructor!
    end
end

end
