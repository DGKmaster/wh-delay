function tau = get_torque(q0, q1, qd)
    %% PID regulator coefficients
    P = 0.04;
    D = 0.00;

    tau = P * (q1 - q0) + D * qd;
