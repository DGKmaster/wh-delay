%! 1 Prepare environment
%! %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% 1.1 Clear workspace
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
close all;
clc;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% 1.2 Run parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% i - ideal, trajectory
%% r - real, after modelling
%% i_points
%% r_points
%% i_pose
%% r_pose
%% all
run_plot = "all r_points";

%% zero
%% gravity
run_torque = "gravity";
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%! %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%! 2 Delay modelling
%! %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% 2.1 Time parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Simulation step
time_step = 0.001;
%% Number of simulation steps
iterations = 1000;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% 2.2 Create circle shape trajectory
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
robot = model();

%% Circle radius
r = 0.1;
%% Distance from robot base
y = repmat(0.25, 1, iterations);

%% Calculate inverse kinematics for circle trajectory
x = r * cos((1:iterations)./iterations * 2 * pi);
z = r * sin((1:iterations)./iterations * 2 * pi);

R = r2t(eul2r(0, 0, 0));
t = transl([x', y', z']);
t(1, 1, :) = 0;
t(2, 2, :) = 0;
t(3, 3, :) = 0;
t(4, 4, :) = 0;

cq_pose = R + t;
q_pose = robot.ikunc(cq_pose);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% 2.3 Init simulation parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Initial state
q_now = q_pose(1, :);
qd_now = [0 0 0];
qdd_now = [0 0 0];

%% History
q_history = zeros(iterations, 3);
qd_history = zeros(iterations, 3);
qdd_history = zeros(iterations, 3);

time_history = zeros(iterations, 1);

torque_history = zeros(iterations, 3);
tau_gravity_history = zeros(iterations, 3);

%% Number of indices to delay
num_delay = 0;

q_delay = q_now;
qd_delay = qd_now;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% 2.3 Forward dynamics
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for i = 1:iterations
    %% Delay
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% Get delay sensor value index
    delay = i - num_delay - 1;

    %% Send initial position value during simulation start
    if delay < 1
        q_delay = q_now;
        qd_delay = qd_now;
    else
        q_delay = q_history(delay, :);
        qd_delay = qd_history(delay, :);
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    %% Torque calculate
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% Gravity force
    tau_gravity = robot.gravload(q_now);
    tau_gravity_history(i, :) = tau_gravity;
    if run_torque == "gravity"
        torque = tau_gravity;
    end

    %% Zero torque to check gravity forces
    zero_torque = zeros(1,3);
    if run_torque == "zero"
        torque = zero_torque;
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    %% Modelling
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    qdd_now = ( robot.accel(q_now, qd_now, torque) )';

    %% Integrate value of speed and position
    qd_now = qd_now + qdd_now * time_step;
    q_now = q_now + qd_now * time_step;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    %% Logging
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% Write history to plot
    q_history(i,:) = q_now;
    qd_history(i, :) = qd_now;
    qdd_history(i, :) = qdd_now;
    torque_history(i, :) = torque;
    time_history(i, :) = time_step * i;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
end

xyz_history = robot.fkine(q_history);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%! %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%! 3 Plotting part
%! %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if contains(run_plot, "all")
    figure(2);
    subplot(3,1,1); plot(time_history, q_history(:,1)); ylabel('Joint 1 (rad)'); title('Position');
    subplot(3,1,2); plot(time_history, q_history(:,2)); ylabel('Joint 2 (rad)');
    subplot(3,1,3); plot(time_history, q_history(:,3)); ylabel('Joint 3 (rad)'); xlabel('Time (s)'); 

    figure(3);
    subplot(3,1,1); plot(time_history, torque_history(:,1)); ylabel('Torque 1 (Nm)'); title('Motor torque');
    subplot(3,1,2); plot(time_history, torque_history(:,2)); ylabel('Torque 2 (Nm)');
    subplot(3,1,3); plot(time_history, torque_history(:,3)); ylabel('Torque 3 (Nm)'); xlabel('Time (s)'); 

    figure(4);
    subplot(3,1,1); plot(time_history, tau_gravity_history(:,1)); ylabel('Torque 1 (Nm)'); title('Gravity torque');
    subplot(3,1,2); plot(time_history, tau_gravity_history(:,2)); ylabel('Torque 2 (Nm)');
    subplot(3,1,3); plot(time_history, tau_gravity_history(:,3)); ylabel('Torque 3 (Nm)'); xlabel('Time (s)'); 
end

figure(5);

if contains(run_plot, "i_points")
    plotp([x; y; z]);
    hold on;
end

if contains(run_plot, "r_points")
    plotp(xyz_history.tv);
    hold on;
end

if contains(run_plot, "i_pose")
    robot.plot(q_pose);
    hold on;
end

if contains(run_plot, "r_pose")
    robot.plot(q_history);
end
%! %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
